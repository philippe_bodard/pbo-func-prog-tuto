import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

/**
 * Created by philippebodard on 2018-12-27.
 */
public class MainComparator {


    public static void main(String... args ) {


        Person philip,clonePhil,laurent,laurentOlder;
        int resultCmp;

        philip= new Person("Philippe","Bodard",54);
        clonePhil = new Person("Philippe","Bodard",54);
        laurent = new Person("Laurent","Bodard",51);
        laurentOlder = new Person("Laurent","Bodard",52);



        Comparator<Person> cmpAge = (p1,p2) -> p2.getAge() - p1.getAge();
        Comparator<Person> cmpFirstName = (p1,p2) -> p2.getFirstName().compareTo(p1.getFirstName());
        Comparator<Person> cmpLastname = (p1,p2) -> p2.getLastName().compareTo(p1.getLastName());

        Function<Person,Integer> f1 = p -> p.getAge();
        Function<Person,String> f2 = p -> p.getFirstName();
        Function<Person,String> f3 = p -> p.getLastName();

        Comparator<Person> cmpPersonAge = Comparator.comparing(Person::getAge);
        System.out.println("philip has the same age as clonePhil " + cmpPersonAge.compare(philip,clonePhil));
        Comparator<Person> cmpPersonFirstName = Comparator.comparing(Person::getFirstName);
        System.out.println("philip is the same firstname as laurent " + cmpPersonFirstName.compare(philip,laurent));


        Comparator<Person> cmp = Comparator.comparing(Person::getLastName)
                                 .thenComparing(Person::getFirstName)
                                 .thenComparing(Person::getAge);


        System.out.println("value of cmp.compare(p1,p2) = 0 => p1 == p2 ");
        System.out.println("value of cmp.compare(p1,p2) < 0 => p1 > p2 ");
        System.out.println("value of cmp.compare(p1,p2) > 0 => p1 < p2 ");

        System.out.println("philip:"+philip);
        System.out.println("clonePhil:"+clonePhil);
        System.out.println("laurent:"+laurent);
        System.out.println("laurentOlder:"+laurentOlder);

        resultCmp = cmp.compare(philip,clonePhil);
        System.out.println("value of cmp.compare(philip,clonePhil) = " + resultCmp);
        resultCmp = cmp.compare(philip,laurent);
        System.out.println("value of cmp.compare(philip,laurent) = " + resultCmp);
        resultCmp = cmp.compare(laurent,laurentOlder);
        System.out.println("value of cmp.compare(laurent,laurentOlder) = " + resultCmp);


    }
}
