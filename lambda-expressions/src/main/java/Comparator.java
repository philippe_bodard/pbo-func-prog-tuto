import java.util.function.Function;

/**
 * Created by philippebodard on 2018-12-27.
 */

@FunctionalInterface
public interface Comparator<T> {

    public int compare(T t1, T t2);

    default Comparator<T> thenComparing(Comparator<T> cmp) {

        return (p1,p2) -> compare(p1,p2) == 0 ? cmp.compare(p1,p2) : compare(p1,p2);
    }

    default Comparator<T> thenComparing(Function<T,Comparable> f) {

        Comparator<T> cmp = comparing(f);

        return thenComparing(cmp);
    }


    public static <U> Comparator<U> comparing(Function<U,Comparable> f) {

        return (p1,p2) -> f.apply(p2).compareTo(f.apply(p1));

    }


}
