import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by philippebodard on 2019-01-01.
 */
public class MainBuildingStreams {

    public static void main(String[] args) {

        List<Integer>   ints = Arrays.asList(0,1,2,3,4,5);

        Stream<Integer> stream= ints.stream();

        stream.forEach(System.out::println);

        /* generate five times the string "one" */
        Stream<String> streamOfStrings = Stream.generate(() ->"one");
        streamOfStrings.limit(5).forEach(System.out::println);

        /* iterates five times with a crowing string of "+" */
        Stream<String> streamOfStrings2 = Stream.iterate("+",s ->s + "+");
        streamOfStrings2.limit(5).forEach(System.out::println);

        /* random local strings */
        IntStream streamOfInt = ThreadLocalRandom.current().ints();
        streamOfInt.limit(5).forEach(System.out::println);

    }


}
