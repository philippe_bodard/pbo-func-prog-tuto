import java.util.*;
import java.util.function.Function;

/**
 * Created by philippebodard on 2018-12-27.
 */
public class MainApiCollection {


    public static void main(String... args ) {

        InHabitant i1= new InHabitant("Alice",23);
        InHabitant i2= new InHabitant("Brian",56);
        InHabitant i3= new InHabitant("Chelsea",46);
        InHabitant i4= new InHabitant("David",28);
        InHabitant i5= new InHabitant("Erica",37);
        InHabitant i6= new InHabitant("Francisco",18);

        List<InHabitant> people = new ArrayList(Arrays.asList(i1,i2,i3,i4,i5,i6));

        people.removeIf(p -> p.getAge() < 30);

        people.replaceAll(inhabitant -> new InHabitant(inhabitant.getName().toUpperCase(),inhabitant.getAge()));

        people.sort(Comparator.comparing(InHabitant::getAge).reversed());

        people.forEach(p -> System.out.println(p));

        City paris = new City("Paris");
        City shanghai = new City("Shanghai");
        City newYork = new City("New York");

        Map<City,List<InHabitant>> map = new HashMap<>();

        System.out.println("People from paris : " + map.getOrDefault(paris,Collections.EMPTY_LIST));

        Map<City,List<InHabitant>> map1 = new HashMap<>();
        map1.computeIfAbsent(newYork,inHabitants -> new ArrayList<>()).add(i1);
        map1.computeIfAbsent(shanghai,inHabitants -> new ArrayList<>()).add(i2);
        map1.computeIfAbsent(shanghai,inHabitants -> new ArrayList<>()).add(i3);

        System.out.println("map1");
        map1.forEach((city, inHabitants) -> System.out.println(city + " " + inHabitants));


        Map<City,List<InHabitant>> map2 = new HashMap<>();
        map2.computeIfAbsent(shanghai,inHabitants -> new ArrayList<>()).add(i4);
        map2.computeIfAbsent(paris,inHabitants -> new ArrayList<>()).add(i4);
        map2.computeIfAbsent(paris,inHabitants -> new ArrayList<>()).add(i5);

        System.out.println("map2");
        map2.forEach((city, inHabitants) -> System.out.println(city + " " + inHabitants));

        System.out.println("merge map1 to map2");

        map2.forEach(
                (city,inHabitants) ->
                        map1.merge(
                                city,inHabitants,
                                (existingInHabitants,newInHabitants) -> {
                                    existingInHabitants.addAll(newInHabitants);
                                    return existingInHabitants;
                                }
                        )
        );

        System.out.println("resulting map1");
        map1.forEach((city, inHabitants) -> System.out.println(city + " " + inHabitants));


    }
}
