import java.util.function.Function;

/**
 * Created by philippebodard on 2018-12-28.
 */
public class MainPredicate {


    public static void main(String... args) {


        Predicate<String> f1 = s -> s.length() < 20;
        Predicate<String> f2 = s -> s.length() > 5;
        Predicate<String> f3 = s -> s.length() < 3;
        Predicate<String> f1Af2 = f1.and(f2);
        Predicate<String> f2Of3 = f2.or(f3);


        String  val1 = "good",
                val2 = "good morning",
                val3 = "goog morning gentlemen",
                val4 = "ok",
                assertionF1AF2 = "5 < length of string < 20 ",
                assertionF2OF3 = "length > 5 or length < 3 ";

        System.out.println(val1 + " asserts "+ assertionF1AF2 + " : " + f1Af2.test(val1));
        System.out.println(val2 + " asserts "+ assertionF1AF2 + " : " + f1Af2.test(val2));
        System.out.println(val3 + " asserts "+ assertionF1AF2 + " : " + f1Af2.test(val3));

        System.out.println(val1 + " asserts "+ assertionF2OF3 + " : " + f2Of3.test(val1));
        System.out.println(val2 + " asserts "+ assertionF2OF3 + " : " + f2Of3.test(val2));
        System.out.println(val3 + " asserts "+ assertionF2OF3 + " : " + f2Of3.test(val3));


        Predicate<String> f5 = Predicate.isEqualTo("yes");
        Predicate<Integer> f6 = Predicate.isEqualTo(10);

        System.out.println("no" + " is equal to yes : "+ f5.test("no"));
        System.out.println("yes" + " is equal to yes : "+ f5.test("yes"));
        System.out.println(5 + " is equal to 10 : "+ f6.test(5));
        System.out.println(10 + " is equal to 10 : "+ f6.test(10));




    }

}